{
  "port": 8098,
  "monochrome": true,
  "replaceWithFile": [
    {
      "from": /^http:\/\/blog.hatena.ne.jp\/-\/blog_style\/\d+\/[a-f0-9]+$/,
      "to":   "dist/the.nyarla.net.css",
      "mime": "text/css; charset=utf-8"
    }
  ]
}
